LilyPond engraving of [Schreck](https://en.wikipedia.org/wiki/Gustav_Schreck)’s reconstruction of Bach’s Concerto in G minor (BWV 1056R) for [Mutopia](http://mutopiaproject.org/).

Only the the second movement has been typeset by now. Feel free to complete the other movements, but please [contact](http://me.digitalwords.net/) me before for coordination.
